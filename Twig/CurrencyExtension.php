<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Twig;

use Gaia\Bundle\AdminBundle\Currency\CurrencyConverterInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class CurrencyExtension.
 *
 * @author Jérôme Fath
 */
final class CurrencyExtension extends AbstractExtension
{
    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * @var CurrencyConverterInterface
     */
    private $currencyConverter;

    /**
     * CurrencyExtension constructor.
     *
     * @param RequestStack               $requestStack
     * @param CurrencyConverterInterface $currencyConverter
     */
    public function __construct(RequestStack $requestStack, CurrencyConverterInterface $currencyConverter)
    {
        $this->requestStack = $requestStack;
        $this->currencyConverter = $currencyConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return [
            new TwigFilter('sylius_currency_symbol', [$this, 'convertCurrencyCodeToSymbol']),
        ];
    }

    /**
     * @param string $code
     *
     * @return string
     */
    public function convertCurrencyCodeToSymbol(string $code): string
    {
        return $this->convertCurrencyCodeToSymbol(
            $code, $this->requestStack->getCurrentRequest()->getLocale()
        );
    }
}
