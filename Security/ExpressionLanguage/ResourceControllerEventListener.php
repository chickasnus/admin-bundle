<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Security\ExpressionLanguage;

use Sylius\Bundle\ResourceBundle\Event\ResourceControllerEvent;
use Sylius\Component\Resource\Model\ResourceInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class ResourceControllerEventListener.
 *
 * @author Jérôme Fath
 */
final class ResourceControllerEventListener
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var ExpressionLanguage
     */
    private $expressionLanguage;

    /**
     * @var array
     */
    private $configuration;

    /**
     * ExpressionLanguageAuthorizationChecker constructor.
     *
     * @param ContainerInterface $container
     * @param ExpressionLanguage $expressionLanguage
     * @param array              $configuration
     */
    public function __construct(ContainerInterface $container, ExpressionLanguage $expressionLanguage, array $configuration)
    {
        $this->container = $container;
        $this->expressionLanguage = $expressionLanguage;
        $this->configuration = $configuration;
    }

    /**
     * @param ResourceControllerEvent $event
     */
    public function on(ResourceControllerEvent $event): void
    {
        $request = $this->container->get('request_stack')->getMasterRequest();
        if(null === $request) {
            return;
        }

        $expression = $this->configuration[$request->attributes->get('_controller')] ?? null;
        if(null === $expression) {
            return;
        }

        $isGranted = $this->expressionLanguage->evaluate($expression, [
            'auth_checker' => $this->container->get('security.authorization_checker'),
            'token' => $this->container->get('security.token_storage')->getToken(),
            'resource' => $event->getSubject() instanceof ResourceInterface ? $event->getSubject() : null,
        ]);

        if (!$isGranted) {
            throw new AccessDeniedException();
        }
    }
}
