<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Exporter;

use Gaia\Bundle\AdminBundle\Exporter\Extractor\GridExtractorInterface;
use Gaia\Bundle\AdminBundle\Exporter\Parser\DataParserInterface;
use Sylius\Bundle\ResourceBundle\Controller\RequestConfiguration;
use Sylius\Component\Grid\View\GridViewInterface;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CSVGridExporter.
 *
 * @author Jérôme Fath
 */
final class CSVGridExporter implements GridExporterInterface
{
    /**
     * @var GridExtractorInterface
     */
    private $headersGridExtractor;

    /**
     * @var DataParserInterface
     */
    private $dataParser;

    /**
     * CSVGridExporter constructor.
     *
     * @param GridExtractorInterface $headersGridExtractor
     * @param DataParserInterface    $dataParser
     */
    public function __construct(GridExtractorInterface $headersGridExtractor, DataParserInterface $dataParser)
    {
        $this->headersGridExtractor = $headersGridExtractor;
        $this->dataParser = $dataParser;
    }

    /**
     * {@inheritdoc}
     */
    public function handle(RequestConfiguration $requestConfiguration, GridViewInterface $gridView, array $options = []): Response
    {
        $response = new StreamedResponse();

        $response->setCallback(function () use ($gridView, $options) {
            $handle = fopen('php://output', 'w+');

            fputcsv($handle, $this->headersGridExtractor->extract($gridView->getDefinition()), $options['delimiter']);
            $this->dataParser->parse($gridView, $gridView->getData(), function ($data) use ($handle, $options) {
                fputcsv($handle, $data, $options['delimiter']);
            });

            fclose($handle);
        });

        $disposition = HeaderUtils::makeDisposition(HeaderUtils::DISPOSITION_ATTACHMENT, $options['filename']);

        $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
        $response->headers->set('Content-Disposition', $disposition);

        return $response;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver
            ->setRequired(['delimiter', 'filename'])
            ->setAllowedTypes('delimiter', 'string')
            ->setAllowedTypes('filename', 'string')
            ->setDefaults([
                'name' => 'csv',
                'delimiter' => ',',
                'filename' => 'export.csv',
            ])
        ;
    }
}
