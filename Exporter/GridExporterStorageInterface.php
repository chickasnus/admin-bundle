<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Exporter;

/**
 * Interface GridExporterStorageInterface.
 *
 * @author Jérôme Fath
 */
interface GridExporterStorageInterface extends GridExporterInterface
{
    /**
     * @param GridExporterInterface $gridHandler
     */
    public function add(GridExporterInterface $gridHandler): void;

    /**
     * @param GridExporterInterface $gridHandler
     */
    public function remove(GridExporterInterface $gridHandler): void;
}
