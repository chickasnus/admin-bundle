<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Exporter\Parser;

use Closure;
use Pagerfanta\Pagerfanta;
use Sylius\Component\Grid\View\GridViewInterface;

/**
 * Interface DataParserInterface.
 *
 * @author Jérôme Fath
 */
interface DataParserInterface
{
    /**
     * @param GridViewInterface $gridView
     * @param Pagerfanta        $paginator
     * @param Closure           $closure
     */
    public function parse(GridViewInterface $gridView, Pagerfanta $paginator, Closure $closure): void;
}
