# GaiaAdminBundle
This admin bundle use beautiful [SyliusResourceBundle](https://github.com/Sylius/SyliusResourceBundle) to manage easy CRUD and [Webpack Encore](https://github.com/symfony/webpack-encore-bundle) to manage resources.

## Configuration

Configure the templating engine :

```yaml
framework:
    # ...
    templating: { engines: ['twig'] }
```

Configure **webpack_encore** in the config/packages/webpack_encore.yaml file:

```yaml
webpack_encore:
    # ...
    builds:
        gaia-admin: '%kernel.project_dir%/public/build/gaia-admin'
```

## Webpack Encore

Add [workspace](https://yarnpkg.com/lang/en/docs/workspaces/) in your project "package.json" :

```json
 "workspaces": [
        "./vendor/gaia/admin-bundle"
    ]
```

Add in your project "webpack.config.js" :

```js
var config = require('./vendor/gaia/admin-bundle/webpack.config');

...

module.exports = [Encore.getWebpackConfig(), config];
```