<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle;

/**
 * Class GaiaAdminEvents.
 *
 * @author Jérôme Fath
 */
final class GaiaAdminEvents
{
    public const MENU_SIDEBAR = 'gaia_admin.menu.sidebar';

    public const MENU_TOPBAR = 'gaia_admin.menu.topbar';

    private function __construct()
    {
    }
}
