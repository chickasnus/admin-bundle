<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration.
 *
 * @author Jérôme Fath
 */
final class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('gaia_admin');

        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('dashboard')
                    ->children()
                        ->scalarNode('route')->cannotBeEmpty()->end()
                    ->end()
                ->end()
                ->arrayNode('translation')
                    ->canBeDisabled()
                    ->children()
                        ->arrayNode('locales')
                            ->requiresAtLeastOneElement()->scalarPrototype()->end()
                        ->end()
                        ->scalarNode('locale_provider')->defaultValue('gaia_admin.translation_locale_provider')->cannotBeEmpty()->end()
                    ->end()
                ->end()
                ->arrayNode('request')
                    ->canBeDisabled()
                    ->children()
                        ->arrayNode('locales')
                            ->useAttributeAsKey('name')
                            ->requiresAtLeastOneElement()
                            ->scalarPrototype()
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('security')
                    ->children()
                        ->scalarNode('authorization_checker')
                            ->defaultValue('sylius.resource_controller.authorization_checker.disabled')
                            ->cannotBeEmpty()
                        ->end()
                        ->arrayNode('expression_language')
                            ->useAttributeAsKey('name')
                            ->scalarPrototype()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
