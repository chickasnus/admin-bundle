<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\DependencyInjection\Compiler;

use Gaia\Bundle\AdminBundle\Exporter\GridExporterStorageInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class GridExporterCompilerPass.
 *
 * @author Jérôme Fath
 */
final class GridExporterCompilerPass implements CompilerPassInterface
{
    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has(GridExporterStorageInterface::class)) {
            return;
        }

        $definition = $container->findDefinition(GridExporterStorageInterface::class);

        $taggedServices = $container->findTaggedServiceIds('gaia_admin.grid_exporter');
        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('add', [new Reference($id)]);
        }
    }
}
