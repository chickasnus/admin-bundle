<?php

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use Gaia\Bundle\AdminBundle\Locale\LocaleRequestConverterInterface;
use Sylius\Component\Resource\Model\TranslatableInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class TranslatableEventSubscriber.
 *
 * @author Jérôme Fath
 */
final class TranslatableEventSubscriber implements EventSubscriber
{
    /** @var LocaleRequestConverterInterface */
    private $localeRequestConverter;

    public function __construct(LocaleRequestConverterInterface $localeRequestConverter)
    {
        $this->localeRequestConverter = $localeRequestConverter;
    }

    public function getSubscribedEvents()
    {
        return [
            Events::postLoad,
        ];
    }

    public function postLoad(LifecycleEventArgs $args): void
    {
        $entity = $args->getEntity();

        if (!$entity instanceof TranslatableInterface) {
            return;
        }

        $entity->setCurrentLocale(
            $this->localeRequestConverter->convertCurrentToTranslationLocale()
        );
    }
}
