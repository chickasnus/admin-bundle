<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Translation;

/**
 * Interface TranslationLocaleProviderInterface.
 *
 * @author Jérôme Fath
 */
interface TranslationLocaleProviderInterface extends \Sylius\Component\Resource\Translation\Provider\TranslationLocaleProviderInterface
{
    /**
     * @param string $defaultLocaleCode
     * @param array  $definedLocalesCodes
     *
     * @return TranslationLocaleProviderInterface
     */
    public function setLocalesCodes(string $defaultLocaleCode, array $definedLocalesCodes): self;
}
