const Encore = require('@symfony/webpack-encore');

Encore.reset();

Encore
    .addEntry('main', './vendor/sylius/ui-bundle/Resources/private/sass/main.scss')
    .addEntry('semantic', './vendor/gaia/admin-bundle/Resources/private/scss/semantic.scss')
    .addEntry('ui', ['./vendor/sylius/ui-bundle/Resources/private/js/app.js', './vendor/sylius/ui-bundle/Resources/private/js/sylius-auto-complete.js'])
    .addEntry('admin', './vendor/gaia/admin-bundle/Resources/private/js/app.js')
    .setPublicPath('/build/gaia-admin/')
    .setOutputPath('public/build/gaia-admin/')
    .splitEntryChunks()
    .enableSingleRuntimeChunk()
    .cleanupOutputBeforeBuild()
    .enableBuildNotifications(true,  function(options) {
        options.title = 'Webpack GaiaAdminBundle';
    })
    .enableSourceMaps(!Encore.isProduction())
    .enableSassLoader()
    .configureBabel(() => {}, {
        useBuiltIns: 'usage',
        corejs: 3
    })
    .autoProvidejQuery()
;

const config = Encore.getWebpackConfig();
config.name = 'gaia-admin';

module.exports = config;