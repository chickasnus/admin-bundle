<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Locale;

/**
 * Interface LocaleRequestInterface
 *
 * @author Jérôme Fath
 */
interface LocaleRequestInterface
{
    public function getCurrentLocale(): string;

    public function getLocales(bool $mapping = false): array;

    public function hasLocale(string $locale): bool;

    public function add(string $requestLocale, string $translationLocale): void;

    public function remove(string $requestLocale): void;
}