<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Locale;

use InvalidArgumentException;

/**
 * Class LocaleRequestConverter
 *
 * @author Jérôme Fath
 */
final class LocaleRequestConverter implements LocaleRequestConverterInterface
{
    /** @var LocaleRequestInterface */
    private $localeRequest;

    public function __construct(LocaleRequestInterface $localeRequest)
    {
        $this->localeRequest = $localeRequest;
    }

    public function convertCurrentToTranslationLocale(): string
    {
        return $this->convertToTranslationLocale($this->localeRequest->getCurrentLocale());
    }

    public function convertToTranslationLocale(string $locale): string
    {
        $this->assertLocale($locale);

        return $this->localeRequest->getLocales(true)[$locale];
    }

    private function assertLocale(string $locale): void
    {
        if(!in_array($locale, $this->localeRequest->getLocales())) {
            throw new InvalidArgumentException(sprintf('Locale "%s" is not available to convert', $locale));
        }
    }
}