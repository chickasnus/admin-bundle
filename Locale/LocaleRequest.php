<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Locale;

use InvalidArgumentException;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class LocaleRequest
 *
 * @author Jérôme Fath
 */
final class LocaleRequest implements LocaleRequestInterface
{
    /** @var RequestStack */
    private $requestStack;

    /** @var array */
    private $locales;

    public function add(string $requestLocale, string $translationLocale): void
    {
        if(!$this->hasLocale($requestLocale)) {
            $this->locales[$requestLocale] = $translationLocale;
        }
    }

    public function remove(string $requestLocale): void
    {
        if($this->hasLocale($requestLocale)) {
            unset($this->locales[$requestLocale]);
        }
    }

    public function __construct(RequestStack $requestStack, array $defaultLocales)
    {
        $this->requestStack = $requestStack;
        $this->locales = array_combine($defaultLocales, $defaultLocales);

        if(empty($this->locales)) {
            throw new InvalidArgumentException('At least one locale must be defined');
        }
    }

    public function getCurrentLocale(): string
    {
        if(null === $this->requestStack->getCurrentRequest()) {
            return array_key_first($this->locales);
        }
        return $this->requestStack->getCurrentRequest()->getLocale();
    }

    public function getLocales(bool $mapping = false): array
    {
        return false === $mapping ? array_keys($this->locales) : $this->locales;
    }

    public function hasLocale(string $locale): bool
    {
        return isset($this->locales[$locale]);
    }
}