<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Locale;

use Symfony\Component\Intl\Languages;
use Symfony\Component\Intl\Locales;
use Webmozart\Assert\Assert;

/**
 * Class LocaleConverter.
 *
 * @author Jérôme Fath
 */
final class LocaleConverter implements LocaleConverterInterface
{
    /**
     * {@inheritdoc}
     */
    public function convertNameToCode(string $name, ?string $locale = null): string
    {
        $code = array_search($name, Languages::getNames($locale), true);
        Assert::string($code, sprintf('Cannot find code for "%s" locale name', $name));

        return $code;
    }

    /**
     * {@inheritdoc}
     */
    public function convertCodeToName(string $code, ?string $locale = null): string
    {
        $name = Locales::getName($code, $locale);
        Assert::string($name, sprintf('Cannot find name for "%s" locale code', $code));

        return $name;
    }
}
