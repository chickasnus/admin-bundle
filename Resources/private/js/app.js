import 'semantic-ui-css/components/api';
import 'semantic-ui-css/components/dropdown';
import 'semantic-ui-css/components/transition';
import './sylius-compound-form-errors';

(function($) {
    $(document).ready(function () {
        $('table thead th.sortable').on('click', function () {
            window.location = $(this).find('a').attr('href');
        });

        $('select.dropdown.semantic').dropdown();

        $('.sylius-autocomplete').autoComplete();

        $('.sylius-tabular-form').addTabErrors();
        $('.ui.accordion').addAccordionErrors();
    });
})(jQuery);