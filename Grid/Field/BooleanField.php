<?php

/*
 * This file is part of the Gaia package.
 *
 * (c) Jérôme Fath
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace Gaia\Bundle\AdminBundle\Grid\Field;

use Sylius\Component\Grid\DataExtractor\DataExtractorInterface;
use Sylius\Component\Grid\Definition\Field;
use Sylius\Component\Grid\FieldTypes\FieldTypeInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class BooleanField.
 *
 * @author Jérôme Fath
 */
final class BooleanField implements FieldTypeInterface
{
    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var DataExtractorInterface
     */
    private $dataExtractor;

    /**
     * Boolean constructor.
     *
     * @param TranslatorInterface    $translator
     * @param DataExtractorInterface $dataExtractor
     */
    public function __construct(TranslatorInterface $translator, DataExtractorInterface $dataExtractor)
    {
        $this->translator = $translator;
        $this->dataExtractor = $dataExtractor;
    }

    /**
     * {@inheritdoc}
     */
    public function render(Field $field, $data, array $options)
    {
        $value = $this->dataExtractor->get($field, $data);

        if (true === $value) {
            return $this->translator->trans('sylius.ui.yes_label');
        }

        return $this->translator->trans('sylius.ui.no_label');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired('template');
        $resolver->setAllowedTypes('template', 'string');
    }
}
